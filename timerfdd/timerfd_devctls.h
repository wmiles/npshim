#ifndef timerfd_devctls_h_included
#define timerfd_devctls_h_included

#include <devctl.h>
#include <time.h>

typedef struct timerdefn {
    struct itimerspec ts;
    int flags;    
} timerdefn_t;

#define TIMERFD_CMD_CODE      1
#define TIMERFD_DEVCTL_CREATE_TIMER __DIOT(_DCMD_MISC,  TIMERFD_CMD_CODE + 0, clockid_t)
#define TIMERFD_DEVCTL_SET_TIMER __DIOTF(_DCMD_MISC,  TIMERFD_CMD_CODE + 1, timerdefn_t )
#define TIMERFD_DEVCTL_GET_TIMER __DIOF(_DCMD_MISC, TIMERFD_CMD_CODE + 2, struct itimerspec )


#endif
