/* timerfdd -- Resource manager for timerfd
 * Based on QNX resource manager sample
 */


#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <devctl.h>

#include "timerfd_devctls.h"

/* Use an extended ocb */
struct ocb;
#define IOFUNC_OCB_T struct ocb

#include <sys/iofunc.h>
#include <sys/dispatch.h>
#include <sys/neutrino.h>
#include <sys/resmgr.h>


// Offset into dispatch_t where the chid is stored
#define DISPATCH_CHID_OFFSET 0x1C
static inline int get_chid(dispatch_t* dpp) {
    return *(int*)((char*)dpp + DISPATCH_CHID_OFFSET);
}


#define NUM_CLIENTS_PER_BLOCK 15
struct blocked_clients {
    struct blocked_clients* next;
    int rcvid[NUM_CLIENTS_PER_BLOCK];  // Unused entries are zeroed
};


/* Our OCB type */
typedef struct ocb {
    iofunc_ocb_t    base_ocb;
    iofunc_notify_t notify[3];
    struct blocked_clients* blocked_clients;
    int             count;
    struct sigevent sigev;
    int             timer_allocated;
    timer_t         timer;
} ocb_t;



static void options (int argc, char **argv);
static int timer_fired( message_context_t* ctp, int code, unsigned flags, void* handle );
static struct blocked_clients* add_blocked_client(struct blocked_clients* active, int client_rcvid);
static void reply_to_blocked_clients(struct ocb* ocb);

/* A resource manager mainly consists of callbacks for POSIX
 * functions a client could call. In the example, we have
 * callbacks for the open(), read() and write() calls. More are
 * possible. If we don't supply own functions (e.g. for stat(),
 * seek(), etc.), the resource manager framework will use default
 * system functions, which in most cases return with an error
 * code to indicate that this resource manager doesn't support
 * this function.*/

/* These prototypes are needed since we are using their names
 * in main(). */

static int io_open (resmgr_context_t *ctp, io_open_t  *msg, RESMGR_HANDLE_T *handle, void *extra);
static int io_read (resmgr_context_t *ctp, io_read_t  *msg, RESMGR_OCB_T *ocb);
static int io_notify( resmgr_context_t *ctp, io_notify_t *msg, RESMGR_OCB_T *ocb);
static int io_close_dup( resmgr_context_t *ctp, io_close_t* msg, RESMGR_OCB_T *ocb);
static int io_unblock(resmgr_context_t *ctp, io_pulse_t  *msg, RESMGR_OCB_T *ocb);
static int io_devctl(resmgr_context_t *ctp, io_devctl_t *msg, RESMGR_OCB_T *ocb);

/* OCB management functions */
static IOFUNC_OCB_T *ocb_calloc (resmgr_context_t *ctp, IOFUNC_ATTR_T  *device);
static void ocb_free (IOFUNC_OCB_T  *ocb);


/*
 * Our connect and I/O functions - we supply two tables
 * which will be filled with pointers to callback functions
 * for each POSIX function. The connect functions are all
 * functions that take a path, e.g. open(), while the I/O
 * functions are those functions that are used with a file
 * descriptor (fd), e.g. read().
 */

static resmgr_connect_funcs_t  connect_funcs;
static resmgr_io_funcs_t       io_funcs;

/* Our OCB management functions */
static iofunc_funcs_t ocb_funcs = {
    _IOFUNC_NFUNCS,
    ocb_calloc,
    ocb_free
};

iofunc_mount_t          mountpoint = { 0, 0, 0, 0, &ocb_funcs };


/*
 * Our dispatch, resource manager, and iofunc variables
 * are declared here. These are some small administrative things
 * for our resource manager.
 */

static dispatch_t              *dpp;
static resmgr_attr_t           rattr;
static dispatch_context_t      *ctp;
static iofunc_attr_t           ioattr;
static int coid;

static char    *progname = "timerfdd";
static char    *mountpt    = "/dev/timerfd";
static int     optv;                               // -v for verbose operation

int main (int argc, char **argv)
{
	int pathID;

	printf ("%s:  starting...\n", progname);

	/* Check for command line options (-v) */
	options (argc, argv);

	/* Allocate and initialize a dispatch structure for use
	 * by our main loop. This is for the resource manager
	 * framework to use. It will receive messages for us,
	 * analyze the message type integer and call the matching
	 * handler callback function (i.e. io_open, io_read, etc.) */
	dpp = dispatch_create ();
	if (dpp == NULL) {
		fprintf (stderr, "%s:  couldn't dispatch_create: %s\n",
				progname, strerror (errno));
		exit (1);
	}

	/* Set up the resource manager attributes structure. We'll
	 * use this as a way of passing information to
	 * resmgr_attach(). The attributes are used to specify
	 * the maximum message length to be received at once,
	 * and the number of message fragments (iov's) that
	 * are possible for the reply.
	 * For now, we'll just use defaults by setting the
	 * attribute structure to zeroes. */
	memset (&rattr, 0, sizeof (rattr));

	/* Now, let's intialize the tables of connect functions and
	 * I/O functions to their defaults (system fallback
	 * routines) and then override the defaults with the
	 * functions that we are providing. */
	iofunc_func_init (_RESMGR_CONNECT_NFUNCS, &connect_funcs,
			_RESMGR_IO_NFUNCS, &io_funcs);
	/* Now we override the default function pointers with
	 * some of our own coded functions: */
	connect_funcs.open = io_open;
	io_funcs.read = io_read;
	io_funcs.notify = io_notify;
	io_funcs.close_dup = io_close_dup;
    io_funcs.unblock = io_unblock;
	io_funcs.devctl = io_devctl;

	/* Initialize the device attributes for the particular
	 * device name we are going to register. It consists of
	 * permissions, type of device, owner and group ID */
	iofunc_attr_init (&ioattr, S_IFCHR | 0666, NULL, NULL);
	ioattr.mount = &mountpoint;

	/* Next we call resmgr_attach() to register our device name
	 * with the process manager, and also to let it know about
	 * our connect and I/O functions. */
	pathID = resmgr_attach (dpp, &rattr, mountpt,
			_FTYPE_ANY, 0, &connect_funcs, &io_funcs, &ioattr);
	if (pathID == -1) {
		fprintf (stderr, "%s:  couldn't attach pathname: %s\n",
				progname, strerror (errno));
		exit (1);
	}

	/* Now we allocate some memory for the dispatch context
	 * structure, which will later be used when we receive
	 * messages. */
	ctp = dispatch_context_alloc (dpp);
	

	/* Bind a connection to the dispatch channel, so we can send
	 * timer pulses. */
	coid = ConnectAttach(0, getpid(), get_chid(dpp), _NTO_SIDE_CHANNEL, 0);
	if (coid == -1) {
		fprintf (stderr, "%s:  couldn't self connect: %s\n",
				progname, strerror (errno));
		exit (1);
	}
	

	/* Done! We can now go into our "receive loop" and wait
	 * for messages. The dispatch_block() function is calling
	 * MsgReceive() under the covers, and receives for us.
	 * The dispatch_handler() function analyzes the message
	 * for us and calls the appropriate callback function. */
	while (1) {
		if ((ctp = dispatch_block (ctp)) == NULL) {
			fprintf (stderr, "%s:  dispatch_block failed: %s\n",
					progname, strerror (errno));
			exit (1);
		}
		/* Call the correct callback function for the message
		 * received. This is a single-threaded resource manager,
		 * so the next request will be handled only when this
		 * call returns. Consult our documentation if you want
		 * to create a multi-threaded resource manager. */
		dispatch_handler (ctp);
	}
}

/*
 *  io_open
 *
 * We are called here when the client does an open().
 * In this simple example, we just call the default routine
 * (which would be called anyway if we did not supply our own
 * callback), which creates an OCB (Open Context Block) for us.
 * In more complex resource managers, you will want to check if
 * the hardware is available, for example.
 */

int
io_open (resmgr_context_t *ctp, io_open_t *msg, RESMGR_HANDLE_T *handle, void *extra)
{
	if (optv) {
		printf ("%s:  in io_open\n", progname);
	}

	return (iofunc_open_default (ctp, msg, handle, extra));
}

/*
 *  io_read
 *
 *  At this point, the client has called the library read()
 *  function, and expects zero or more bytes.  Since this is
 *  the /dev/Null resource manager, we return zero bytes to
 *  indicate EOF -- no more bytes expected.
 */

/* The message that we received can be accessed via the
 * pointer *msg. A pointer to the OCB that belongs to this
 * read is the *ocb. The *ctp pointer points to a context
 * structure that is used by the resource manager framework
 * to determine whom to reply to, and more. */
	int
io_read (resmgr_context_t *ctp, io_read_t *msg, RESMGR_OCB_T *ocb)
{
	int status;
	int nonblock;

	if (optv) {
		printf ("%s:  in io_read: %08X %d\n", progname, (intptr_t)ocb, ocb->count);
	}

	/* Here we verify if the client has the access
	 * rights needed to read from our device */
	if ((status = iofunc_read_verify(ctp, msg, &ocb->base_ocb, &nonblock)) != EOK) {
	    printf ("%s:  in io_read verify: %d\n", status);
		return (status);
	}

	/* We check if our read callback was called because of
	 * a pread() or a normal read() call. If pread(), we return
	 * with an error code indicating that we don't support it.*/
	if ((msg->i.xtype & _IO_XTYPE_MASK) != _IO_XTYPE_NONE) {
	    printf ("%s:  in io_read xtype: %d\n", status);
		return (ENOSYS);
	}

	if (ocb->count) {
        /* Reply to the client with the current count, then clear it */
        printf ("%s:  in io_read reply\n");
        MsgReply(ctp->rcvid, sizeof(ocb->count), &ocb->count, sizeof(ocb->count));
        ocb->count = 0;

    	return (_RESMGR_NOREPLY);
    } else {
        if (nonblock) {
            return EAGAIN;
        } else {
            ocb->blocked_clients = add_blocked_client(ocb->blocked_clients, ctp->rcvid);
            return (_RESMGR_NOREPLY);
        }
    }
}


int
io_notify( resmgr_context_t *ctp, io_notify_t *msg,
           RESMGR_OCB_T *ocb)
{
    int             trig = 0;

    /*
     * 'trig' will tell iofunc_notify() which conditions are
     * currently satisfied.  'dattr->nitems' is the number of
     * messages in our list of stored messages.
    */

    if (ocb->count > 0)
        trig |= _NOTIFY_COND_INPUT; /* we have some data available */

    /*
     * iofunc_notify() will do any necessary handling, including
     * adding the client to the notification list if need be.
    */
    int armed, rv;
    rv = iofunc_notify( ctp, msg, ocb->notify, trig, NULL, &armed);
    
	if (optv) {
		printf ("%s:  in io_notify: %08X %d -- %d\n", progname, (intptr_t)ocb, ocb->count, armed);
	}
	
	return rv;
}



int io_close_dup( resmgr_context_t *ctp, io_close_t* msg, RESMGR_OCB_T *ocb)
{
    /*
     * A client has closed its file descriptor or has terminated.
     * Unblock any threads waiting for notification, then
     * remove the client from the notification list.
     */
     
	if (optv) {
		printf ("%s:  in io_close_dup: %08X %d\n", progname, (intptr_t)ocb, ocb->count);
	}
     

    iofunc_notify_trigger_strict( ctp, ocb->notify, INT_MAX, IOFUNC_NOTIFY_INPUT);
    iofunc_notify_trigger_strict( ctp, ocb->notify, INT_MAX, IOFUNC_NOTIFY_OUTPUT );
    iofunc_notify_trigger_strict( ctp, ocb->notify, INT_MAX, IOFUNC_NOTIFY_OBAND );

    iofunc_notify_remove(ctp, ocb->notify);

    reply_to_blocked_clients(ocb);
    // Release the last ocb clients
    free(ocb->blocked_clients);
    ocb->blocked_clients = NULL;

    return (iofunc_close_dup_default(ctp, msg, &ocb->base_ocb));
}

int io_unblock( resmgr_context_t *ctp, io_pulse_t * msg, RESMGR_OCB_T *ocb)
{
    int status;
    
    if (optv) {
		printf ("%s:  in io_unblock: %08X %d\n", progname, (intptr_t)ocb, ocb->count);
	}

    
    /*
     * A client has requested an unblock
     */
     if((status = iofunc_unblock_default(ctp,msg,&ocb->base_ocb)) != _RESMGR_DEFAULT) {
         return status;
     }

     /* Check if rcvid is still valid and still has an unblock
        request pending. */
     struct _msg_info info;
     if (MsgInfo(ctp->rcvid, &info) == -1 || !(info.flags & _NTO_MI_UNBLOCK_REQ)) {
     	return _RESMGR_NOREPLY;
     }
     
     /* Locate the client in our list of blocked clients and send them a reply */
     struct blocked_clients* current = ocb->blocked_clients;
     while (current) {
         int i;
         for (i = 0; i < NUM_CLIENTS_PER_BLOCK; ++i) {
             if (current->rcvid[i] == ctp->rcvid) {
                 current->rcvid[i] = 0;
                 MsgReply(ctp->rcvid, -EINTR, NULL, 0);
                 return _RESMGR_NOREPLY; 
             }
         }
         
         current = current->next;
     }
    
    // ????? we got an unblock request for an unregistered client ???
    return _RESMGR_NOREPLY; 
}

static int io_devctl(resmgr_context_t *ctp, io_devctl_t *msg, RESMGR_OCB_T *ocb) {

    int     nbytes, status;

    union {  /* See note 1 */
        clockid_t  clkid;
        timerdefn_t td;
        struct itimerspec ts;
    } *rx_data;


    /*
     Let common code handle DCMD_ALL_* cases.
     You can do this before or after you intercept devctls, depending
     on your intentions.  Here we aren't using any predefined values,
     so let the system ones be handled first. See note 2.
    */
    if ((status = iofunc_devctl_default(ctp, msg, &ocb->base_ocb)) != _RESMGR_DEFAULT) {
        return(status);
    }
    status = nbytes = 0;

    /*
     Note this assumes that you can fit the entire data portion of
     the devctl into one message.  In reality you should probably
     perform a MsgReadv() once you know the type of message you
     have received to get all of the data, rather than assume
     it all fits in the message.  We have set in our main routine
     that we'll accept a total message size of up to 2 KB, so we
     don't worry about it in this example where we deal with ints.
    */

    /* Get the data from the message. See Note 3. */
    rx_data = _DEVCTL_DATA(msg->i);
    
	if (optv) {
		printf ("%s:  in io_devctl %d: %08X %d\n", progname, msg->i.dcmd, (intptr_t)ocb, ocb->count);
	}
    

    switch (msg->i.dcmd) {
    case TIMERFD_DEVCTL_CREATE_TIMER:
        if (ocb->timer_allocated == 0)
        {
            // Attempt to create timer
            status = timer_create(rx_data->clkid, &ocb->sigev, &ocb->timer);
            if (status == -1) {
                status = errno;
            } else {
                ocb->timer_allocated = 1;
            }
            nbytes = 0;
        } else {
            status = ENOSYS;
        }
        break;

    case TIMERFD_DEVCTL_GET_TIMER:
        if (ocb->timer_allocated) {
            status = timer_gettime(ocb->timer, &rx_data->ts);
            if (status == -1) status = errno;
            nbytes = sizeof(rx_data->ts);
        } else {
            status = ENOSYS;
        }
        break;

    case TIMERFD_DEVCTL_SET_TIMER:
        if (ocb->timer_allocated) {
            status = timer_settime(ocb->timer, rx_data->td.flags, &rx_data->td.ts, &rx_data->ts);
            if (status == -1) {
                status = errno;
            } else {
                nbytes = sizeof(rx_data->ts);
                ocb->count = 0;
            }
        } else {
            status = ENOSYS;
        }
        break;

    default:
        return(ENOSYS);
    }

    /* Clear the return message. Note that we saved our data past
       this location in the message. */
    memset(&msg->o, 0, sizeof(msg->o));

    /*
     If you wanted to pass something different to the return
     field of the devctl() you could do it through this member.
     See note 5.
    */
    msg->o.ret_val = status;

    /* Indicate the number of bytes and return the message */
    msg->o.nbytes = nbytes;
    return(_RESMGR_PTR(ctp, &msg->o, sizeof(msg->o) + nbytes));
}


/*
 * ocb_calloc
 *
 *  The purpose of this is to give us a place to allocate our own OCB.
 *  It is called as a result of the open being done
 *  (e.g., iofunc_open_default causes it to be called). We
 *  registered it through the mount structure.
 */
IOFUNC_OCB_T*
ocb_calloc (resmgr_context_t *ctp, IOFUNC_ATTR_T *device)
{
    struct ocb *ocb;
    struct sched_param sched;

    if (!(ocb = calloc (1, sizeof (*ocb)))) {
        return 0;
    }

    // Initial state, no triggered events
    ocb->count = -1;

    // Allocate a pulse for the timer
    int pulse_code = pulse_attach(dpp, MSG_FLAG_ALLOC_PULSE, 0, timer_fired, ocb);
    if (pulse_code == -1) {
        // Unable to allocate pulse (!)
        return 0;
    }

    // Bind the pulse to the sigevent
    pthread_getschedparam(pthread_self(), NULL, &sched);
    SIGEV_PULSE_INIT(&ocb->sigev, coid, sched.sched_priority, pulse_code, 0 );   // TODO: consider sched_currpriority instead ??

    // We don't allocate the timer here; we require a devcontrol so we know the right clockid
    ocb->timer_allocated = 0;
    
    IOFUNC_NOTIFY_INIT(ocb->notify);
    
	if (optv) {
		printf ("%s:  in ocb_alloc: %08X %d\n", progname, (intptr_t)ocb, ocb->count);
	}
    

    return (ocb);
}

/*
 * ocb_free
 *
 * The purpose of this is to give us a place to free our OCB.
 * It is called as a result of the close being done
 * (e.g., iofunc_close_ocb_default causes it to be called). We
 * registered it through the mount structure.
 */
void
ocb_free (IOFUNC_OCB_T *ocb)
{
	if (optv) {
		printf ("%s:  in ocb_free: %08X %d\n", progname, (intptr_t)ocb, ocb->count);
	}
    
    
    if (ocb->timer_allocated) {
        timer_delete(ocb->timer);
    }
    pulse_detach(dpp, ocb->sigev.sigev_code, 0);
    while (ocb->blocked_clients) {
        struct blocked_clients* fm = ocb->blocked_clients;
        ocb->blocked_clients = fm->next;
        free(fm);
    }
    free (ocb);
}


/*
 *  options
 *
 *  This routine handles the command-line options.
 *  We support:
 *      -n <path>   set mount point
 *      -v      verbose operation
 */

	void
options (int argc, char **argv)
{
	int     opt;

	optv = 0;

	while (optind < argc) {
		while ((opt = getopt (argc, argv, "n:v")) != -1) {
			switch (opt) {
			    case 'n':
			        mountpt = optarg;
			        break;

				case 'v':
					optv = 1;
					break;
			}
		}
	}
}




int
timer_fired( message_context_t* ctp, int code, unsigned flags, void* handle )
{
    struct ocb* ocb = (struct ocb*) handle;

    if (optv) {
		printf ("%s:  in timer_fired: %08X %d\n", progname, (intptr_t)ocb, ocb->count);
	}

    ocb->count++;

    // Trigger any poll() type notifies
    if (IOFUNC_NOTIFY_INPUT_CHECK(ocb->notify, ocb->count, 0)) {
        printf("trigger\n");
        iofunc_notify_trigger(ocb->notify, ocb->count, IOFUNC_NOTIFY_INPUT);
    };

    // Unblock any clients
    reply_to_blocked_clients(ocb);

    return 0;
}

struct blocked_clients* add_blocked_client(struct blocked_clients* active, int client_rcvid) {
    struct blocked_clients* current = active;
    int i;
    while (current) {
        for (i = 0; i < NUM_CLIENTS_PER_BLOCK; ++i) {
            if (current->rcvid[i] == 0) {
                current->rcvid[i] = client_rcvid;
                return active;
            }
        }
        current = current->next;
    }

    current = calloc(1, sizeof(struct blocked_clients));
    if (current) {
        current->next = active;
        current->rcvid[0] = client_rcvid;
    }
    return current;
}


void reply_to_blocked_clients(struct ocb* ocb) {
    int clear_count = 0;
    while (ocb->blocked_clients) {
        int i;
        for (i = 0; i < NUM_CLIENTS_PER_BLOCK; ++i) {
            if (ocb->blocked_clients->rcvid[i] != 0) {
                if (ocb->count) {
                    MsgReply(ocb->blocked_clients->rcvid[i], 0, &ocb->count, sizeof(ocb->count));                    
                    clear_count = 1;
                } else {
                    MsgReply(ocb->blocked_clients->rcvid[i], -ESRCH, NULL, 0);
                }
            }
        }

        // If we're the last one, leave it -- reduces allocator hits
        if (ocb->blocked_clients->next == NULL) {
            memset(&ocb->blocked_clients, 0, sizeof(struct blocked_clients));
        } else {
            struct blocked_clients* fm = ocb->blocked_clients;
            ocb->blocked_clients = ocb->blocked_clients->next;
            free(fm);
        };
    }
    
    if (clear_count) ocb->count = 0;
};