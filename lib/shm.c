#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#include "sys/shm.h"

// In sysv, the kernel ought to already know the memory size just from the addr.
// We aren't so lucky; so we have to track it ourselves.
struct shm_mapping {
    void* addr;
    size_t size;
};

static pthread_mutex_t active_maps_mutex = PTHREAD_MUTEX_INITIALIZER;
static struct shm_mapping* active_maps = NULL;
static size_t active_maps_size = 0;


// Utilities
struct shm_mapping* add_mapping();
struct shm_mapping* get_mapping(const void* addr);


/* Shared memory control operation.  */
int shmctl (int shmid, int cmd, struct shmid_ds *buf) {
    // Unsupported
    errno = EINVAL;
    return -1;
}

/* Get shared memory segment.  */
/* Lame implementation, leaks fds */
int shmget (key_t key, size_t size, int shmflg) {
    /* no support for IPC_PRIVATE */
    if (key == IPC_PRIVATE) {
        errno = EINVAL;
        return -1;
    }

    /* Construct a name suitable for shm_open */
    char name[32];
    snprintf(name,sizeof(name),"/sysv_%08X",key);

    /* Sort out the flags */
    /* Lame: always open for RW; we map with the right permissions */
    int flags = O_RDWR;
    if (shmflg & IPC_CREAT) {
        flags |= O_CREAT;
    }
    if (shmflg & IPC_EXCL) {
        flags |= O_EXCL;
    }
    int perm = shmflg & 0xFF;   // UNIX permissions

    /* Try to open that entry */
    int fd = shm_open(name, flags, perm);
    if (fd < 0) {
        return fd;
    }

    /* Success */
    if (shmflg & IPC_CREAT) {
        // Check the size of the entry
        struct stat xs;
        fstat(fd, &xs);
        if (xs.st_size != size) {
            ftruncate(fd, size);
        }
    }

    return fd;
}

/* Attach shared memory segment.  */
void *shmat (int shmid, const void *shmaddr, int shmflg) {
    if (shmid < 0) {
        errno = EINVAL;
        return NULL;
    }

    /* Look up the size */
    struct stat xs;
    fstat(shmid, &xs);



    void* result = mmap((void*) shmaddr,
        xs.st_size,
        (shmflg & SHM_RDONLY) ? (PROT_READ) : (PROT_READ | PROT_WRITE),
        MAP_SHARED | (shmaddr ? MAP_FIXED : 0),
        shmid,
        0);

    if (result != MAP_FAILED) {
        /* Save the size for deallocation later */
        pthread_mutex_lock(&active_maps_mutex);

        struct shm_mapping* mapping = add_mapping();
        mapping->addr = result;
        mapping->size = xs.st_size;
        pthread_mutex_unlock(&active_maps_mutex);
    };

    return result;

}

/* Detach shared memory segment.  */
int shmdt (const void *shmaddr) {
    struct shm_mapping* mapping;
    size_t size;

    /* Look up the size; if found, mark it as freed */
    pthread_mutex_lock(&active_maps_mutex);
    mapping = get_mapping(shmaddr);

    if (!mapping) {
        pthread_mutex_unlock(&active_maps_mutex);
        errno = EINVAL;
        return -1;
    }

    size = mapping->size;
    mapping->addr = MAP_FAILED;
    pthread_mutex_unlock(&active_maps_mutex);

    /* And do the deed */
    return munmap((void*)shmaddr, size);
}




/* Utilities */
struct shm_mapping* add_mapping() {
    size_t idx;
    /* Scan for an empty slot */
    for (idx = 0; idx < active_maps_size; ++idx) {
        if (active_maps[idx].addr == MAP_FAILED) {
            return &active_maps[idx];
        }
    }

    /* Nope, gotta make some more space */
    idx = min(idx*2, 16);
    struct shm_mapping* new_map = realloc(active_maps, idx * sizeof(struct shm_mapping));
    if (!new_map) { return NULL; };

    /* Initialize the new block */
    memset(&new_map[active_maps_size], -1, (idx - active_maps_size) * sizeof(struct shm_mapping));
    active_maps = new_map;
    new_map = active_maps + active_maps_size;
    active_maps_size = idx;
    return new_map;
}


struct shm_mapping* get_mapping(const void* addr) {
   size_t idx;
    /* Linear scan */
    for (idx = 0; idx < active_maps_size; ++idx) {
        if (active_maps[idx].addr == addr) {
            return &active_maps[idx];
        }
    }
    return NULL;
};

