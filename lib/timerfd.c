
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/timerfd.h>
#include <errno.h>
#include <stdio.h>

#include "timerfd_devctls.h"


/* Return file descriptor for new interval timer source.  */
int timerfd_create (clockid_t clock_id, int flags) {
    
    const char* path = getenv("TIMERFD_DEVICE");
    if (!path) path = "/dev/timerfd";
    
    // Open an fd
    int oflags = O_RDONLY 
              | ((flags & TFD_NONBLOCK) ? O_NONBLOCK : 0)
              | ((flags & TFD_CLOEXEC) ? O_CLOEXEC : 0);
    
    int fd = open(path, oflags );
    if (fd == -1) {
        return -1;  //errno already set
    }
    
    // Attempt to open the clock
    
    // devctl (create)
    int tmp;
    int result = devctl(fd, TIMERFD_DEVCTL_CREATE_TIMER, &clock_id, sizeof(clock_id), &tmp );

    if (result == 0) {
        return fd;
    } else {        
        close(fd);
        errno = result;
        return -1;        
    }
}


/* Set next expiration time of interval timer source UFD to UTMR.  If
   FLAGS has the TFD_TIMER_ABSTIME flag set the timeout value is
   absolute.  Optionally return the old expiration time in OTMR.  */
int timerfd_settime (int ufd, int flags,
			    const struct itimerspec *utmr,
			    struct itimerspec *otmr)
{
    if (utmr == NULL) {
        errno = EINVAL;
        return -1;
    };
    
    struct timerdefn td = {*utmr, (flags & TFD_TIMER_ABSTIME) ? TIMER_ABSTIME : 0 };
    int info = 0;
     
    int result = devctl(ufd, TIMERFD_DEVCTL_SET_TIMER, &td, sizeof(td), &info );
    
    if ((result == 0) && (otmr != NULL)) {
        *otmr = td.ts;
    };
    
    if (result) {
        errno = result;
        return -1;
    } else {
        return 0;
    }
}

/* Return the next expiration time of UFD.  */
int timerfd_gettime (int ufd, struct itimerspec *otmr) {
    if (!otmr) { errno = EINVAL; return -1; };
    int info = 0;
     
    int result = devctl(ufd, TIMERFD_DEVCTL_GET_TIMER, otmr, sizeof(struct itimerspec), &info );
    
    if (result) {
        errno = result;
        return -1;
    } else {
        return 0;
    }    
}
