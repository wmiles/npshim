// Fake SHM header for QNX

#ifndef _SYS_SHM_H
#define _SYS_SHM_H	1

#include <sys/types.h>
#include <sys/ipc.h>

/* The following System V style IPC functions implement a shared memory
   facility.  The definition is found in XPG4.2.  */

struct shmid_ds;

#define SHM_RDONLY 1

/* Shared memory control operation.  */
extern int shmctl (int shmid, int cmd, struct shmid_ds *buf) ;

/* Get shared memory segment.  */
extern int shmget (key_t key, size_t size, int shmflg) ;

/* Attach shared memory segment.  */
extern void *shmat (int shmid, const void *shmaddr, int shmflg) ;

/* Detach shared memory segment.  */
extern int shmdt (const void *shmaddr) ;

#endif /* sys/shm.h */
